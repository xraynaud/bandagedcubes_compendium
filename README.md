# A Compendium of Bandaged 3x3x3 Cubes.

This is a shiny app that lists configurations for Bandanged 3x3x3 cubes published on the http://www.twistypuzzles.com/ forum. Puzzles are ordered alphabetically with a possibility to select puzzles having particular bandages and, when available, a particular difficulty level. 

The app also have a simple scrambling function. If you have a CubeTwist kit, you can select the puzzle, scramble it, reproduce the pattern on your cubetwist kit and solve the puzzle without having to scramble the real cube first. Note that the scrambling algorithm is VERY simple and might produce very easy puzzles to solve when there are a lot of bandages. 

### Usage

The project use shiny. You must have R (www.r-project.org) and Rstudio (www.rstudio.org) to make it run. To run the application, you can either download the whole project to your computer, or load the runGitLab function available at https://gitlab.com/snippets/1708282 from R and then do a 

    runGitLab("bandagedcubes_compendium","xraynaud")