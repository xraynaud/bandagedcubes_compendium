library(shiny)

source("src/BandagedFunctions.R")
source("src/io.R")
source("src/plot.R")
source("src/scramble.R")
source("src/signature.R")
source("src/ui_functions.R")

server <- (function(input, output) {
  
read.BandagedCubes("data/masterdbv2.xlsx")->cubes
  #  cubes = read.BandagedCubes("masterdbv2.csv")
 
  menu = do.call("rbind",lapply(seq_along(cubes),function(x) {
    tmp = data.frame(
      idx=rep(x,length(cubes[[x]]$names)),
      diff = cubes[[x]]$avgdifficulty,
      bands = ifelse(is.null(names(CountBandages(cubes[[x]])))," ",paste(names(CountBandages(cubes[[x]])),collapse=" "))
      )
  row.names(tmp) = cubes[[x]]$names; return(tmp)
  }))
  
  output$selector <- renderUI({
    selectInput("cube_select","Select Cube:",choices=lapply(split(buildmenu(menu,input)$idx,row.names(buildmenu(menu,input))),unlist),selected=1,selectize=F,size=20,width=200)
  })
  
  selected <- reactiveValues(cube=cubes[[1]])
  graph <- reactiveValues(type = 1)
  colours <- reactiveValues(scheme = 0)
  observeEvent(input$cube_select, {
    selected$cube = cubes[[as.numeric(input$cube_select)]]
  })
#  selected_cube <- reactive({
#    if (is.null(input$cube_select)) {
#      thecube = min(menu[menu$diff>=input$diffrange[1]&menu$diff<=input$diffrange[2] &apply(do.call("cbind",lapply(ifelse(is.null(input$bandages_select),"",input$bandages_select) ,function(x) grepl(x,menu[,3]))),1,all) ,1])
#    } else {
#      thecube = as.numeric(input$cube_select)
#    }
#    return(thecube)
#  })
  
  observeEvent(input$scramblebutton, {
    selected$cube =scramble(selected$cube)
  })
  
  observeEvent(input$reloadButton, {
    read.BandagedCubes("data/masterdbv2.xlsx")->cubes
    save(cubes,file="data/cubes.RData")
  })
  
  observeEvent(input$graphtype,{
    graph$type = (graph$type+1)%%2
  })
  observeEvent(input$cscheme,{
    colours$scheme = (colours$scheme+1)%%2
  })
  
  #  output$cube <- renderRglwidget({
  #    rgl.open(useNULL=T)
  #    rgl::par3d(FOV=60)
  #rgl::observer3d(10, 0, 10,auto=T)
  #    plot3d(cube_cur,al.interior=1,bg="white",bbshininess = 0,bbox=T)
  #    rglwidget()
  #  })
  
  
  output$cubename<-renderText({paste("<font size=\"5\">",paste(selected$cube$name,collapse=", "),"</font>")})
  output$difficulty<-renderPlot(
    diffbar(selected$cube$avgdifficulty)
    )
  output$bandages<-renderTable(CountBandages(selected$cube),colnames = F,bordered = T)
  output$signature<-renderText(paste("signature:",selected$cube$signature))
  output$inventor<-renderText(paste("Inventor:", ifelse(length(unique(selected$cube$inventor[!is.na(selected$cube$inventor)]))==1, selected$cube$inventor[!is.na(selected$cube$inventor)][1], paste(selected$cube$inventor[!is.na(selected$cube$inventor)],collapse = ", "))))
  
  output$plot2d <-renderPlot({
    par(mar=c(0.5,0.5,0.5,0.5))
    if (graph$type  == 1) {
      plot(selected$cube,colorschemes[[colours$scheme+1]])
    } else {
      plotIso(selected$cube,colorschemes[[colours$scheme+1]])
    }
  })
  
  output$scramble <- renderText(tostring_rotations(selected$cube$rotations))
  output$notes <- renderText(ifelse(!is.na(selected$cube$notes),paste(selected$cube$notes),""))
  output$museumlink <- renderText({
    if (sum(!is.na(selected$cube$museumlink))>0) {
      if (sum(!is.na(selected$cube$museumlink))==1) {
        paste("More info in the ","<a href=\"",paste(selected$cube$museumlink[1]),"\">Twisty puzzle forum Museum</a>")
      } else {
        paste("More info in the Twisty puzzle forum Museum",paste("<a href=\"",paste(selected$cube$museumlink),"\">here</a>", sep="",collapse = ", "),sep=": ")
      }
    }
  })
  output$footer<-renderText(paste("<br/><footer><i>Last updated:", format(as.Date(file.info("data/cubes.RData")$mtime), "%d %B %Y"),"  <br/>",dim(menu)[1],"cubes in the database (", length(cubes)," unique cube configurations)</i></footer>"))
})
